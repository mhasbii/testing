# Generated by Django 2.1.1 on 2020-10-22 09:31

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('story6', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Kegiatan',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Peserta',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(max_length=30)),
                ('kegiatan', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='story6.Kegiatan')),
            ],
        ),
        migrations.RemoveField(
            model_name='person',
            name='kegiatan',
        ),
        migrations.DeleteModel(
            name='Activity',
        ),
        migrations.DeleteModel(
            name='Person',
        ),
    ]
