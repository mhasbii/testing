from django.urls import path
from . import views

app_name = 'story_6'

urlpatterns = [
    path('', views.story6),
    path('post-kegiatan', views.post_kegiatan),
    path('post-peserta/<int:id_kegiatan>', views.post_peserta),
]
