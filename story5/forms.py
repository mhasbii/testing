from django import forms


class matkulform(forms.Form):
    error_messages = {
        'required': 'This field is required.'
    }
    attrs = {
        'class': 'form-control'
    }
    nama = forms.CharField(widget=forms.TextInput(attrs=attrs), label='Mata Kuliah ', max_length=50, required=True)
    dosen = forms.CharField(widget=forms.TextInput(attrs=attrs), label='Nama Dosen ', max_length=50, required=True)
    jumlahsks = forms.IntegerField(widget=forms.NumberInput(attrs=attrs), label='Jumlah SKS ',required=True)
    deskripsi = forms.CharField(widget=forms.TextInput(attrs=attrs), label='Deskripsi Singkat ', required=True)
    semestertahun = forms.CharField(widget=forms.TextInput(attrs=attrs), label='Semester / Tahun ', max_length=50, required=True)
    ruangan = forms.CharField(widget=forms.TextInput(attrs=attrs), label='Ruang Kelas ', max_length=50, required=True)