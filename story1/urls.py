from django.conf.urls import url
from django.urls import path
from .views import *

urlpatterns = [
	path('', index, name='index'),
	path('story1', story1, name='story1'),
	path('resume', resume, name='resume'),
	path('story3', story3, name='story3'),
]