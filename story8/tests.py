from django.test import TestCase, Client
from django.urls import resolve, reverse
from . import views
# Create your tests here.

class UnitTestStory8(TestCase) :
    def test_url_status_code(self) :
        response = Client().get('/story8/')
        self.assertEqual(response.status_code, 200)

    def test_template(self) :
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'story8.html')

    def test_func_in_views(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, views.story8)
        
    def test_pemanggilan_tautan(self):
        response = Client().get('/story8/?q=bebasapaaja')
        self.assertEqual(response.status_code, 200)