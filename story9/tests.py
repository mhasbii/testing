from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.contrib import auth
import json

class TestStory9(TestCase):

    def test_to_login_page(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code, 302)
    
    def test_login_page(self):
        response = Client().get('/story9/auth/')
        self.assertTemplateUsed(response, 'login.html')

    def test_to_sign_up(self):
        userInput = {'username' : 'mhasbii', 'password' : '12345', 'email' : 'hasbi@gmail.com'}
        response = Client().post('/story9/api/v1/signup/', json.dumps(userInput), content_type="application/json")
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'status': 200,
            'message': 'Your account is now ready to use'}
        )

    def test_login_failed(self):
        userInput = {'username' : 'mhasbii', 'password' : '1234'}
        response = Client().post('/story9/api/v1/login/', json.dumps(userInput), content_type="application/json")
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'status': 401,
            'message': 'Username does not exist'}
        )

    def test_bad_request_api(self):
        response = Client().get('/story9/api/v1/login/')
        self.assertEqual(response.status_code, 400)

        response = Client().get('/story9/api/v1/signup/')
        self.assertEqual(response.status_code, 400)